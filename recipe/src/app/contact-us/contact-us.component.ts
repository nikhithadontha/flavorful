import { Component } from '@angular/core';
// import emailjs, { EmailJSResponseStatus } from '@emailjs/browser';

import emailjs, { EmailJSResponseStatus } from 'emailjs-com';

@Component({
  selector: 'app-contact-us',
  templateUrl: './contact-us.component.html',
  styleUrls: ['./contact-us.component.css']
})
export class ContactUsComponent {

  constructor() {
    // Initialize EmailJS with your User ID (public key)
    emailjs.init('s2a9zqykpEJCKO5Ij'); // Replace with your EmailJS User ID
  }

  sendEmail(e: Event) {
    e.preventDefault();
    // Use the EmailJS SDK to send the form data
    emailjs.sendForm('service_nmm362y', 'template_1y74yxg', e.target as HTMLFormElement, 's2a9zqykpEJCKO5Ij')
      .then((response: EmailJSResponseStatus) => {
        console.log('Email sent successfully :', response);
        alert('Email sent successfully!');
        // Optionally reset the form after successful submission
        (e.target as HTMLFormElement).reset();
      }, (error) => {
        console.error('Failed to send email:', error);
        alert('Failed to send email. Please try again later.');
      });
  }
}


// import { Component } from '@angular/core';
// import emailjs, { EmailJSResponseStatus } from 'emailjs-com'; // Import EmailJSResponseStatus explicitly

// @Component({
//   selector: 'app-contact-us',
//   templateUrl: './contact-us.component.html',
//   styleUrls: ['./contact-us.component.css']
// })
// export class ContactUsComponent {

//   constructor() {
//     // Initialize EmailJS with your User ID (public key)
//     emailjs.init('usqimmckzpymjvjc'); // Replace with your EmailJS User ID
//   }

//   sendEmail(e: Event) {
//     e.preventDefault();
//     // Use the EmailJS SDK to send the form data
//     emailjs.sendForm('service_adleupr', 'template_41r28yh', e.target as HTMLFormElement, 'usqimmckzpymjvjc')
//       .then((response: EmailJSResponseStatus) => {
//         console.log('Email sent successfully:', response);
//         alert('Email sent successfully!');
//         // Optionally reset the form after successful submission
//         (e.target as HTMLFormElement).reset();
//       }, (error: any) => { // Explicitly specify 'error' as 'any' type
//         console.error('Failed to send email:', error);
//         alert('Failed to send email. Please try again later.');
//       });
//   }
// }

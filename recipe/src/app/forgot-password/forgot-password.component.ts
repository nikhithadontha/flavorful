

// import { Component, OnInit } from '@angular/core';
// import { Router } from '@angular/router';
// import { UserService } from '../user.service';
// import { ToastrService } from 'ngx-toastr';

// @Component({
//   selector: 'app-forgot-password',
//   templateUrl: './forgot-password.component.html',
//   styleUrls: ['./forgot-password.component.css']
// })
// export class ForgotPasswordComponent implements OnInit {
//   emailId: string = '';

//   constructor(
//     private router: Router,
//     private service: UserService,
//     private toaster: ToastrService
//   ) {}

//   ngOnInit(): void {}

//   sendOTP() {
//     if (this.emailId) {
//       this.service.forgetOtp(this.emailId).subscribe(
//         (data: string) => {
//           console.log(data);
//           this.toaster.success(data);
//           localStorage.setItem('emailId', this.emailId);
//           this.router.navigate(['verify-otp']);
//         },
//         (error: any) => {
//           console.error(error);
//           this.toaster.error('Error sending OTP');
//         }
//       );
//     } else {
//       this.toaster.error('Please enter a valid email');
//     }
//   }
// }


import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { UserService } from '../user.service';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-forgot-password',
  templateUrl: './forgot-password.component.html',
  styleUrls: ['./forgot-password.component.css']
})
export class ForgotPasswordComponent implements OnInit {
  emailId: string = '';

  constructor(
    private router: Router,
    private service: UserService,
    private toaster: ToastrService
  ) {}

  ngOnInit(): void {}

  sendOTP() {
    if (this.emailId) {
      this.service.forgetOtp(this.emailId).subscribe(
        (data: string) => {
          console.log(data);
          this.toaster.success('OTP sent to email successfully!');
          localStorage.setItem('emailId', this.emailId);
          this.router.navigate(['verify-otp']);
        },
        (error: any) => {
          console.error(error);
          this.toaster.error('Error sending OTP');
        }
      );
    } else {
      this.toaster.error('Please enter a valid email');
    }
  }
}



// import { Component } from '@angular/core';
// import { NgToastService } from 'ng-angular-popup';
// import { Router } from '@angular/router';
// import { UserService } from '../user.service';

// @Component({
//   selector: 'app-reset-password',
//   templateUrl: './reset-password.component.html',
//   styleUrls: ['./reset-password.component.css']
// })
// export class ResetPasswordComponent {
//   emailId: string = ''; // Assuming you have a way to capture user's email id for the reset process
//   newPassword: string = '';
//   confirmPassword: string = '';

//   constructor(
//     private toast: NgToastService,
//     private router: Router,
//     private userService: UserService
//   ) {}

//   resetPassword(formValue: any) {
//     if (formValue.newPassword !== formValue.confirmPassword) {
//       this.toast.error({ detail: 'Error', summary: 'Passwords do not match', duration: 5000 });
//       return;
//     }

//     this.userService.resetPassword(this.emailId, formValue.newPassword).subscribe(
//       (response: any) => {
//         this.toast.success({ detail: 'Success', summary: 'Password reset successfully', duration: 5000 });
//         this.router.navigate(['/login']);
//       },
//       (error: any) => {
//         this.toast.error({ detail: 'Error', summary: 'Failed to reset password', duration: 5000 });
//       }
//     );
//   }
// }
import { Component } from '@angular/core';
import { NgToastService } from 'ng-angular-popup';
import { Router } from '@angular/router';
import { UserService } from '../user.service';

@Component({
  selector: 'app-reset-password',
  templateUrl: './reset-password.component.html',
  styleUrls: ['./reset-password.component.css']
})
export class ResetPasswordComponent {
  newPassword: string = '';
  confirmPassword: string = '';

  constructor(
    private toast: NgToastService,
    private router: Router,
    private userService: UserService
  ) {}

  resetPassword(formValue: any) {
    if (formValue.newPassword !== formValue.confirmPassword) {
      this.toast.error({ detail: 'Passwords do not match', summary: 'Error', duration: 5000 });
      return;
    }

    this.userService.resetPassword(formValue.emailId, formValue.newPassword).subscribe(
      (response: any) => {
        this.toast.success({ detail: 'Password reset successfully', summary: 'Success', duration: 5000 });
        this.router.navigate(['login']);
      },
      (error: any) => {
        this.toast.error({ detail: 'Failed to reset password', summary: 'Error', duration: 5000 });
      }
    );
  }
}

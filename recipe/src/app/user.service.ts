

// import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
// import { Injectable } from '@angular/core';
// import { Observable, Subject } from 'rxjs';

// @Injectable({
//   providedIn: 'root'
// })
// export class UserService {
//   private apiUrl = 'http://localhost:8085'; // Base URL for the API

//   isUserLoggedIn: boolean;
//   loginStatus: any;

//   constructor(private http: HttpClient) {
//     this.isUserLoggedIn = false;
//     this.loginStatus = new Subject();
//   }

//   setUserLoggedIn() {
//     this.isUserLoggedIn = true;
//     this.loginStatus.next(true);
//   }

//   setUserLoggedOut() {
//     this.isUserLoggedIn = false;
//     this.loginStatus.next(false);
//   }

//   forgetOtp(emailId: string): Observable<string> {
//     const params = new HttpParams().set('emailId', emailId);
//     return this.http.get<string>(`${this.apiUrl}/forgot-password`, { params });
//   }

//   otpVerify(emailId: string, otp: string): Observable<any> {
//     return this.http.post(`${this.apiUrl}/verify-otp`, { emailId, otp });
//   }

//   getLoginStatus(): boolean {
//     return this.isUserLoggedIn;
//   }

//   getUserLoginStatus(): any {
//     return this.loginStatus.asObservable();
//   }

//   // User CRUD Operations
//   getAllUsers() {
//     return this.http.get(`${this.apiUrl}/getAllUsers`);
//   }

//   registerUser(user: any) {
//     return this.http.post(`${this.apiUrl}/addUser`, user);
//   }

//   userLogin(emailId: string, password: string) {
//     return this.http.get(`${this.apiUrl}/userLogin/${emailId}/${password}`).toPromise();
//   }

//   getUserById(userId: number) {
//     return this.http.get(`${this.apiUrl}/getUserById/${userId}`).toPromise();
//   }

//   deleteUserById(userId: number) {
//     return this.http.delete(`${this.apiUrl}/deleteUserById/${userId}`);
//   }

//   updateUser(user: any) {
//     return this.http.put(`${this.apiUrl}/updateUser`, user);
//   }

//   getAllCountries() {
//     return this.http.get('https://restcountries.com/v3.1/all');
//   }

//   saveRecipe(recipe: any): Observable<any> {
//     return this.http.post<any>(`${this.apiUrl}/recipes/addRecipe`, recipe);
//   }

//   getAllRecipes() {
//     return this.http.get(`${this.apiUrl}/recipes/getAllRecipes`);
//   }
// }



import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, Subject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class UserService {
  logout() {
    throw new Error('Method not implemented.');
  }

  isUserLoggedIn: boolean;
  loginStatus: any;

  constructor(private http: HttpClient) {
    this.isUserLoggedIn = false;
    this.loginStatus = new Subject();
  }
  
  setUserLoggedIn() {
    this.isUserLoggedIn = true;
    this.loginStatus.next(true);
  }
  setUserLoggedOut() {
    this.isUserLoggedIn = false;
    this.loginStatus.next(false);
  }
  

  forgetOtp(emailId: string): Observable<any> {
    return this.http.get(`http://localhost:8085/forgot-password?emailId=${emailId}`, { responseType: 'text' });
  }
verifyOtp(email: string, otp: string): Observable<any> {
  const payload = { emailId: email, otp: otp };
  return this.http.post(`http://localhost:8085/verify-otp`, payload);
}
resetPassword(emailId: string, newPassword: string): Observable<any> {
  const payload = { emailId, newPassword };
  return this.http.post(`http://localhost:8085/reset-password`, payload);
}

  getLoginStatus(): boolean {
    return this.isUserLoggedIn;
  }
  
  getUserLoginStatus(): any {
    return this.loginStatus.asObservable();
  }

  getAllUsers() {
    return this.http.get('http://localhost:8085/getAllUsers');
  }

  registerUser(user: any) {
    return this.http.post('http://localhost:8085/addUser', user);
  }

  userLogin(emailId: any, password: any) {
    return this.http.get('http://localhost:8085/userLogin/' + emailId + "/" + password).toPromise()
  }

  getUserById(userId: any) {
    return this.http.get('http://localhost:8085/getUserById/' + userId).toPromise();
  }

  deleteUserById(userId: any) {
    return this.http.delete('http://localhost:8085/deleteUserById/' + userId);
  }

  updateUser(user: any) {
    return this.http.put('http://localhost:8085/updateUser', user);
  }

  getAllCountries() {
    return this.http.get('https://restcountries.com/v3.1/all');
  }

  private apiUrl = 'http://localhost:8085/recipes';

  saveRecipe(recipe: any): Observable<any> {
    return this.http.post<any>(`${this.apiUrl}/addRecipe`, recipe);
  }

  getAllRecipes() {
    return this.http.get('http://localhost:8085/recipes/getAllRecipes');
  }
}

import { Component } from '@angular/core';
import { UserService } from '../user.service';
import { Router, ActivatedRoute } from '@angular/router';
import { NgToastService } from 'ng-angular-popup';

@Component({
  selector: 'app-verify-otp',
  templateUrl: './verify-otp.component.html',
  styleUrls: ['./verify-otp.component.css']
})
export class VerifyOtpComponent {
  email: string = '';

  constructor(
    private service: UserService,
    private router: Router,
    private route: ActivatedRoute,
    private toast: NgToastService
  ) {
    // Retrieve email from localStorage
    this.email = localStorage.getItem('emailId') || '';
  }

  verifyOtp(verifyOtpForm: any) { // Changed to match the template
    const otp = verifyOtpForm.otp;
    if (otp) {
      this.service.verifyOtp(this.email, otp).subscribe(
        (response: any) => {
          this.toast.success({ detail: 'Success', summary: 'OTP Verified', duration: 5000 });
          this.router.navigate(['reset-password'], { queryParams: { email: this.email } });
        },
        (error: any) => {
          this.toast.error({ detail: 'Error', summary: 'Invalid OTP', duration: 5000 });
        }
      );
    } else {
      this.toast.error({ detail: 'Error', summary: 'Please enter OTP', duration: 5000 });
    }
  }
}
